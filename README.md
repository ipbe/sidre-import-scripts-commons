# Import Scripts Commons of the Search Index for Distributed Repositories

This projects provides some common python methods to insert / update into the Backend of the Distributed Resources Search Index. Also some common functions for conversion are provided.

## Requirements

* python3
* see [requirements.txt](requirements.txt)

## Usage

1. Create your own import scripts repository
2. Use a subfolder `python` in your repo
3. Include this module as dependency via a `python/requirements.txt` file
   ```
   https://gitlab.com/oersi/sidre/sidre-import-scripts-commons/-/jobs/artifacts/main/download?job=deploy
   ```
   (If using `main`, you will always use the latest version. Otherwise, use the url of a released artifact)
4. Implement your Import Scripts (also see "Create new import" below)
5. Build your project artifact (also see "GitLab CI example" below)
6. Set your artifact in the Ansible-Scripts (search-index-setup-repo)
    1. example `oerindex_import_scripts_artifact_url: 'https://gitlab.com/TIBHannover/course-catalog/course-catalog-import-scripts/-/jobs/artifacts/main/download?job=deploy'` 

## Configuration

You need to add the following files to your project and adjust your configuration. This can also be done via our Ansible-Scripts.

* `config/config.yml`
    * configure access of search-index-backend
* `config/logging.conf`
    * logging configuration (see https://docs.python.org/3/library/logging.html)

## Features

If you use this module in your import script repo, you can easily access the following features

### Create new import

* new import class should extend `search_index_import_commons.SearchIndexImporter`
    * implement the abstract methods
* new import script has to be stored in your python-root-directory and the filename has to end with `Import.py`
* new import mapping should extend `search_index_import_commons.SearchIndexImportMapping`
* use common functionalities
    * helper methods in `search_index_import_commons.Helpers`
    * OAI-PMH-Reader
    * Sitemap Reader
* Add unit tests in `tests` and use `search_index_import_commons.test.BaseImportTest.py` for easier mocking

### Webservice

Optionally, (some parts of) the script module can be controlled via web service. This is necessary for integration with optional Search Index features (such as the single record update).

[Flask](https://flask.palletsprojects.com/) is used as the framework for this.

The feature can be installed / activated via oersi-setup fully automatically (see https://gitlab.com/oersi/oersi-setup/-/blob/master/ansible/roles/oer-search-index-import-scripts/tasks/webservice.yml) - just activate it via the Ansible-Variable `oerindex_import_scripts_python_webservice_install`. [Gunicorn](https://gunicorn.org/) is used as WSGI HTTP Server here. The requirements for this feature ("Flask", "Flask-Limiter", "gunicorn") are also installed via Ansible.

### GitLab / GitHub import

You can import metadata from GitLab and GitHub repos. For this, there needs to be a topic that is used for filtering resources in the GitLab / GitHub repo. Metadata is read from a file `metadata.yml` that needs to be contained in the repository.

You can provide metadata for a single resource or you can provide a list of metadata for multiple resources in the `metadata.yml`. It is also possible to include other files of the repository in the `metadata.yml` via a statement `!include your-file.yml`. In GitLab repositories it is possible to use a wildcard-include and include multiples files at once this way like `!include resource_metadata/*.yml`.

In your import-scripts implementation, you just need to extend the classes `GitLabImporterBase` / `GitHubImporterBase` for your instance-specific transformation.

## Commands

Execute command from root directory

* Run tests `python3 -m unittest tests/test_**.py`
* Run tests with test-coverage `coverage3 run -m unittest tests/test_**.py`

## FAQ

1. How to execute the tests in my IntelliJ IDE?

   Modify the "Run Configuration" of the test file (right-click on the test-file): set the working-directory to _python_ instead of the default _python/tests_
2. How to test my adjusted script in my local Vagrant VM?

   Start your related Vagrant-VM with `vagrant up` and run your script directly from your IDE / python venv.

## Examples

### GitLab CI example for implementing repositories

```
stages:
  - test
  - deploy

variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
test:
  stage: test
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
      - ~/.cache/pip/
  script:
    - pip3 install -r python/requirements.txt
    - cd python && coverage3 run -m unittest tests/test_*.py && coverage3 xml && cd ..
# deactivated sonar-analysis -> search for "sonarqube" / "sonarcloud" configuration to activate
#    - sonar-scanner
  only:
    - merge_requests
    - master

deploy:
  stage: deploy
  script:
    - echo BUILDNUMBER=${CI_JOB_ID} > build.info
    - echo COMMIT_SHA=${CI_COMMIT_SHORT_SHA} >> build.info
    - date +'TIMESTAMP=%Y%m%d%H%M%S' >> build.info
    - echo SOURCE=${CI_PROJECT_URL}/-/tree/${CI_COMMIT_SHORT_SHA} >> build.info
    - echo BUILD-JOB=${CI_JOB_URL} >> build.info
    - cat build.info
  artifacts:
    paths:
      - build.info
      - python/**
    exclude:
      - python/tests/**
      - python/tests

```
