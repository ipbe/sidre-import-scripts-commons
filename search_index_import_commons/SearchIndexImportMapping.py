import abc
import logging


class SearchIndexImportMapping(abc.ABC):
    def __init_fields__(self):
        self.mappings = {}
        self.missing_mappings = []

    def add_mapping(self, group, mapping, default=None):
        if not hasattr(self, "mappings"):
            self.__init_fields__()
        self.mappings[group] = {"mapping": mapping, "default": default}

    def get(self, group, key):
        if not hasattr(self, "mappings"):
            self.__init_fields__()
        group_mappings = self.mappings.get(group)
        if not group_mappings:
            logging.error("No mapping group '%s'", group)
            return None
        if key in group_mappings["mapping"]:
            result = group_mappings["mapping"].get(key)
        else:
            self.missing_mappings.append({"group": group, "key": key})
            result = group_mappings["default"]
        return result

    def log_missing_keys(self):
        messages = []
        for missing_mapping in self.missing_mappings:
            messages.append("No mapping defined for '{}':'{}'".format(missing_mapping["group"], missing_mapping["key"]))
        for message in sorted(set(messages)):
            logging.info(message)
