from flask import Flask, request
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
import logging.config
from os import getcwd
from urllib.parse import urlparse
import search_index_import_commons.RecordUpdater as RecordUpdater
import logging.config

logging.config.fileConfig(getcwd() + '/config/logging.conf', disable_existing_loggers=False)
app = Flask(__name__, instance_relative_config=True)
limiter = Limiter(app=app, key_func=get_remote_address)


def is_valid_url(url):
    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc])
    except:
        return False


@app.route("/update-record", methods=['POST'])
@limiter.limit("30 per minute")
def update_record_service():
    record_id = request.form['recordId']
    if not record_id or not is_valid_url(record_id):
        return "Invalid url-param 'recordId'", 500
    if RecordUpdater.update_record(record_id):
        return "OK", 200
    else:
        return "FAILED", 500
