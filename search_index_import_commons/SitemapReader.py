import gzip
import logging
import re

import requests
import xml.etree.ElementTree as Xml


class SitemapReader:
    ns = {"xmlns": "http://www.sitemaps.org/schemas/sitemap/0.9"}

    def __init__(self, url, user_agent, url_pattern=None, sitemap_pattern=None):
        self.url = url
        self.user_agent = user_agent
        self.url_pattern = url_pattern
        self.sitemap_pattern = sitemap_pattern

    def __read_urls_from_sitemap__(self, sitemap_url):
        sitemap_response = requests.get(sitemap_url, headers={"User-Agent": self.user_agent})
        if sitemap_response.status_code != 200:
            raise IOError("Could not fetch records from {}: {}".format(sitemap_url, sitemap_response))
        if sitemap_url.endswith('.gz') and isinstance(sitemap_response.content, bytes):
            sitemap = str(gzip.decompress(sitemap_response.content), 'utf-8')
        else:
            sitemap = sitemap_response.content

        xml_response = Xml.fromstring(sitemap)
        urls = []
        if xml_response.tag == "{" + self.ns["xmlns"] + "}" + "sitemapindex":
            sitemap_urls = list(map(lambda s: s.text.strip(), xml_response.findall("xmlns:sitemap/xmlns:loc", self.ns)))
            if self.sitemap_pattern:
                pattern = re.compile(self.sitemap_pattern)
                sitemap_urls = list(filter(lambda url: pattern.search(url), sitemap_urls))
            logging.debug("%s is a sitemap-index -> loading listed sitemaps: %s", self.url, sitemap_urls)
            for s in sitemap_urls:
                urls = urls + self.__read_urls_from_sitemap__(s)
        else:
            urls = list(map(lambda s: s.text.strip(), xml_response.findall("xmlns:url/xmlns:loc", self.ns)))
        if self.url_pattern:
            pattern = re.compile(self.url_pattern)
            urls = list(filter(lambda url: pattern.search(url), urls))
        return urls

    def get_url_locations(self) -> list:
        return self.__read_urls_from_sitemap__(self.url)
