import abc
import base64
import re
import requests
import urllib.parse

from search_index_import_commons.GitHelpers import GitImporterBase, GitConnector


class GitLabConnector(GitConnector):
    def __init__(self, gitlab_base_url):
        self.gitlab_base_url = gitlab_base_url
        self.user_agent = None
        self.default_headers = {}

    def set_default_user_agent(self, user_agent):
        self.user_agent = user_agent
        self.default_headers = {"User-Agent": self.user_agent}

    def get_file_content_from_repo(self, data, filename):
        params = {"ref": data["default_branch"]}
        repo_resp = requests.get(self.gitlab_base_url + "/api/v4/projects/" + str(data["id"]) + "/repository/files/" + urllib.parse.quote_plus(filename), params=params, headers=self.default_headers)
        repo_json = repo_resp.json()
        if repo_resp.status_code == 200 and "content" in repo_json:
            return base64.b64decode(repo_json["content"]).decode("utf-8")
        return None

    def list_files(self, repo_metadata, path):
        params = {"ref": repo_metadata["default_branch"], "per_page": 100, "recursive": False}
        if path is not None:
            params["path"] = path
        repo_resp = requests.get(self.gitlab_base_url + "/api/v4/projects/" + str(repo_metadata["id"]) + "/repository/tree", params=params, headers=self.default_headers)
        if repo_resp.status_code == 200:
            return list(map(lambda x: x["name"], repo_resp.json()))
        return None

    def get_tags(self, repo_metadata):
        tags_resp = requests.get(self.gitlab_base_url + "/api/v4/projects/" + str(repo_metadata["id"]) + "/repository/tags", headers=self.default_headers)
        if tags_resp.status_code == 200:
            return tags_resp.json()
        return None


class GitLabImporterBase(GitImporterBase, abc.ABC):
    """Basic functionalities for GitLab importers. Implementations should inherit from this class."""
    DEFAULT_PROTOCOL = "https://"

    def __init__(self, name, gitlab_domain, converter, topic, default_gitlab_pages_domain=None):
        self.gitlab_domain = gitlab_domain
        self.gitlab_base_url = self.DEFAULT_PROTOCOL + self.gitlab_domain
        super().__init__(GitLabConnector(self.gitlab_base_url))
        self.name = name
        self.topic = topic
        self.default_gitlab_pages_domain = default_gitlab_pages_domain
        self.loaded = False
        self.converter = converter

    def get_name(self):
        return self.name

    def contains_record(self, record_id):
        return record_id.startswith(self.gitlab_base_url + "/")

    def load_single_record(self, record_id):
        headers = {"User-Agent": self.user_agent}
        url_match = re.match("^" + self.gitlab_base_url + "/(.+)", record_id)
        if url_match:
            project_id = urllib.parse.quote_plus(url_match.group(1))
            resp = requests.get(self.gitlab_base_url + "/api/v4/projects/" + project_id, headers=headers)
            if resp.status_code in [200, 404]:
                records = self.check_and_load_metadata(project_id, resp.json()) if resp.status_code == 200 else None
                return {"response_status_code": 404} if not records else records
        return None

    def load_next(self):
        if self.loaded:
            return None
        headers = {"User-Agent": self.user_agent}
        params = {"visibility": "public", "topic": self.topic}
        resp = requests.get(self.gitlab_base_url + "/api/v4/projects", params=params, headers=headers)
        json = resp.json()
        records = []
        for project_data in json:
            project_metadata_records = self.check_and_load_metadata(str(project_data["id"]), project_data)
            records.extend(project_metadata_records)
        self.loaded = True
        return records if len(records) > 0 else None

    def get_default_gitlab_pages_url(self, gitlab_metadata):
        if self.default_gitlab_pages_domain:
            namespace = gitlab_metadata["path_with_namespace"].split("/")[0].lower()
            path = gitlab_metadata["path_with_namespace"].split("/", 1)[-1]
            return self.DEFAULT_PROTOCOL + namespace + "." + self.default_gitlab_pages_domain + "/" + path
        return gitlab_metadata["web_url"]

    def to_search_index_metadata(self, record):
        record_meta = record["resource_metadata"]
        record_meta["git_metadata"] = {
            "identifier": record_meta["url"] if "url" in record_meta else self.get_default_gitlab_pages_url(record["git_repo_metadata"]),
            "source_url": record["git_repo_metadata"]["web_url"],
            "star_count": record["git_repo_metadata"]["star_count"],
            "hasVersion": list(map(lambda x: {"id": record["git_repo_metadata"]["web_url"] + "/-/tree/" + x["name"], "name": x["name"]}, record["git_repo_metadata"]["tags"])) if "tags" in record["git_repo_metadata"] else None
        }
        return self.converter.transform(record_meta, self.get_name(), self.gitlab_base_url)
