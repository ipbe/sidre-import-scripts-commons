import abc
import re
import requests
from search_index_import_commons.GitHelpers import GitImporterBase, GitConnector


def get_default_github_pages_url(github_metadata):
    if github_metadata["has_pages"]:
        namespace = github_metadata["owner"]["login"].lower()
        path = github_metadata["name"]
        return "https://" + namespace + ".github.io/" + path
    return github_metadata["html_url"]


class GitHubConnector(GitConnector):
    def __init__(self):
        self.user_agent = None
        self.default_headers = {}

    def set_api_bearer_token(self, api_bearer_token):
        if api_bearer_token is not None:
            self.default_headers["Authorization"] = "Bearer " + api_bearer_token

    def set_default_user_agent(self, user_agent):
        self.user_agent = user_agent
        self.default_headers["User-Agent"] = self.user_agent

    def get_file_content_from_repo(self, data, filename):
        repo_resp = requests.get("https://raw.githubusercontent.com/" + data["full_name"] + "/" + data["default_branch"] + "/" + filename, headers={"User-Agent": self.user_agent})
        if repo_resp.status_code == 200:
            return repo_resp.text
        return None

    def list_files(self, repo_metadata, path):
        # not supported for GitHub
        return None

    def get_tags(self, repo_metadata):
        tags_resp = requests.get(repo_metadata["tags_url"], headers=self.default_headers)
        if tags_resp.status_code == 200:
            return tags_resp.json()
        return None


class GitHubImporterBase(GitImporterBase, abc.ABC):
    def __init__(self, converter, topic):
        connector = GitHubConnector()
        super().__init__(connector)
        self.page = 0
        self.converter = converter
        self.topic = topic
        self.api_bearer_token = self.config[self.get_name()]["api_bearer_token"] if self.get_name() in self.config and "api_bearer_token" in self.config[self.get_name()] else None
        connector.set_api_bearer_token(self.api_bearer_token)

    def get_name(self):
        return "GitHub"

    def get_github_api_headers(self):
        headers = {"User-Agent": self.user_agent}
        if self.api_bearer_token:
            headers["Authorization"] = "Bearer " + self.api_bearer_token
        return headers

    def contains_record(self, record_id):
        return record_id.startswith("https://github.com/")

    def load_single_record(self, record_id):
        url_match = re.match("https://github.com/(.+/.+)", record_id)
        if url_match:
            project_id = url_match.group(1)
            resp = requests.get("https://api.github.com/repos/" + project_id, headers=self.get_github_api_headers())
            if resp.status_code in [200, 404]:
                records = self.check_and_load_metadata(project_id, resp.json()) if resp.status_code == 200 else None
                return {"response_status_code": 404} if not records else records
        return None

    def load_next(self):
        self.page += 1
        params = {"q": "topic:" + self.topic, "page": str(self.page), "per_page": "100"}
        resp = requests.get("https://api.github.com/search/repositories", params=params, headers=self.get_github_api_headers())
        json = resp.json()
        if len(json["items"]) == 0:
            return None
        records = []
        for project_data in json["items"]:
            project_metadata_records = self.check_and_load_metadata(str(project_data["id"]), project_data)
            records.extend(project_metadata_records)
        return records

    def to_search_index_metadata(self, record):
        record_meta = record["resource_metadata"]
        record_meta["git_metadata"] = {
            "identifier": record_meta["url"] if "url" in record_meta else get_default_github_pages_url(record["git_repo_metadata"]),
            "source_url": record["git_repo_metadata"]["html_url"],
            "star_count": record["git_repo_metadata"]["stargazers_count"],
            "hasVersion": list(map(lambda x: {"id": record["git_repo_metadata"]["html_url"] + "/tree/" + x["name"], "name": x["name"]} ,record["git_repo_metadata"]["tags"])) if "tags" in record["git_repo_metadata"] else None
        }
        return self.converter.transform(record_meta, self.get_name(), "https://github.com")
