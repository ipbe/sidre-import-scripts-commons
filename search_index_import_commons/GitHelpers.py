import abc
import errno
import logging
import os
import re
import yaml
from typing import Optional
from search_index_import_commons.Helpers import flatten
from search_index_import_commons.SearchIndexImporter import SearchIndexImporter


class GitConnector(abc.ABC):
    @abc.abstractmethod
    def get_file_content_from_repo(self, repo_metadata, filename) -> Optional[str]:
        """Get the content of the file as string, if available. Otherwise, None is returned"""
    @abc.abstractmethod
    def list_files(self, repo_metadata, path) -> list:
        """List all files in the given directory 'path'. If 'path' is None, list files of root directory."""
    @abc.abstractmethod
    def get_tags(self, repo_metadata):
        """Get the content of the file as string, if available. Otherwise, None is returned"""
    @abc.abstractmethod
    def set_default_user_agent(self, user_agent):
        """Set the default user agent"""


class GitMetadataLoader:
    class GitYamlContentLoaderConstructor:
        PATH_WILDCARD_PATTERN = re.compile(r"^(.*/)?([*][^/]*)")

        def __init__(self, git_connector, git_repo_metadata):
            self.git_connector = git_connector
            self.git_repo_metadata = git_repo_metadata

        def __call__(self, loader, node):
            filename = loader.construct_scalar(node)

            wildcard_match = re.match(self.PATH_WILDCARD_PATTERN, filename)
            if wildcard_match:
                path = wildcard_match.group(1)
                file_basename_pattern = re.compile(wildcard_match.group(2).replace("*", ".*"))
                repo_files = self.git_connector.list_files(self.git_repo_metadata, path)
                if repo_files is None:
                    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), filename)
                matching_files = list(filter(lambda x: re.match(file_basename_pattern, x), repo_files))
                path = path if path else ""
                return list(filter(None, map(lambda x: self.__get_yaml_content_from_connector__(path + x), matching_files)))

            return self.__get_yaml_content_from_connector__(filename)

        def __get_yaml_content_from_connector__(self, filename):
            resource_content = self.git_connector.get_file_content_from_repo(self.git_repo_metadata, filename)
            if resource_content is None:
                return None
            return yaml.safe_load(resource_content)

    def __init__(self, git_connector):
        self.git_connector = git_connector

    def __get_yaml_loader__(self, git_repo_metadata):
        loader = yaml.SafeLoader
        loader.add_constructor('!include', self.GitYamlContentLoaderConstructor(self.git_connector, git_repo_metadata))
        return loader

    def load_metadata(self, git_repo_id, git_repo_metadata):
        try:
            metadata_content = self.git_connector.get_file_content_from_repo(git_repo_metadata, "metadata.yml")
            if metadata_content is None:
                return []
            metadata = yaml.load(metadata_content, self.__get_yaml_loader__(git_repo_metadata))
            if isinstance(metadata, list):
                return metadata
            return [metadata]
        except Exception as err:
            logging.info("Failed to load metadata for " + git_repo_id)
            logging.debug(err)
        return []


class GitImporterBase(SearchIndexImporter):
    """Basic functionalities for GitLab and GitHub importers."""

    def __init__(self, git_connector):
        super().__init__()
        self.git_connector = git_connector
        self.git_connector.set_default_user_agent(self.user_agent)
        self.metadata_loader = GitMetadataLoader(git_connector)
        self.feature_versions = self.config["git"]["features"]["versions"] if "git" in self.config and "features" in self.config["git"] and "versions" in self.config["git"]["features"] else False

    @abc.abstractmethod
    def should_import(self, metadata) -> bool:
        """Check if the given git metadata should be imported into the search index"""

    def check_and_load_metadata(self, git_repo_id, git_repo_metadata) -> list:
        logging.debug("Checking project " + git_repo_id)
        result = []
        metadata_records = self.metadata_loader.load_metadata(git_repo_id, git_repo_metadata)
        for idx, metadata_record in enumerate(metadata_records):
            if self.should_import(metadata_record):
                result.append({"git_repo_metadata": git_repo_metadata, "resource_metadata": metadata_record})
            else:
                logging.debug(git_repo_id + " (record " + str(idx + 1) + ") should not be imported -> skipping")
        if self.feature_versions and result:
            tags = self.git_connector.get_tags(git_repo_metadata)
            if tags is not None:
                for metadata_record in result:
                    metadata_record["git_repo_metadata"]["tags"] = tags
        return result

    def transform(self, data: list) -> list:
        return flatten(list(filter(None, map(lambda record: self.to_search_index_metadata(record), data))))

    @abc.abstractmethod
    def to_search_index_metadata(self, record):
        """instance specific transformation of a record"""
