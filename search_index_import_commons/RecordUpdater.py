from os.path import basename, isfile, join
from os import getcwd
import glob
import logging
import logging.config
import importlib
import inspect
import sys


def is_import_with_contains_record_check(clazz):
    if not clazz[0].endswith("Import") or inspect.isabstract(clazz[1]):
        return False
    functions = inspect.getmembers(clazz[1], inspect.isfunction)
    return "contains_record" in list(map(lambda f: f[0], functions))


def get_imports_with_contains_record_check():
    result = []
    search_dir = getcwd()
    logging.debug("Checking directory for Import-Modules: " + search_dir)
    modules = glob.glob(join(search_dir, "*Import.py"))
    logging.debug("Testing Import-Modules: " + str(modules))
    for module_name in [basename(f)[:-3] for f in modules if isfile(f)]:
        module = importlib.import_module(module_name)
        classes = inspect.getmembers(module, inspect.isclass)
        result = result + list(filter(lambda clazz: is_import_with_contains_record_check(clazz), classes))
    return result


def update_record(record_identifier):
    logging.info("Starting update for url: %s", record_identifier)
    for import_class in get_imports_with_contains_record_check():
        try:
            importer = import_class[1]()
        except Exception:
            logging.debug(import_class[0] + " cannot be instantiated")
            continue
        if importer.contains_record(record_identifier):
            logging.debug("Using '" + import_class[0] + "' for the update")
            return importer.process_single_update(record_identifier)
    logging.info("The specified URL is not assigned to a connected source -> Cancel")
    return False


if __name__ == "__main__":
    logging.config.fileConfig(getcwd() + '/config/logging.conf', disable_existing_loggers=False)
    if len(sys.argv) < 2:
        logging.info("Method parameter record-url of mainEntityOfPage is mandatory!")
        exit(1)
    res = update_record(sys.argv[1])
    exit(0 if res else 1)
